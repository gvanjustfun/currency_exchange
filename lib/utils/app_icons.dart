import 'package:flutter/cupertino.dart';

class AppIcons {
  static final IconData chevronDown = IconData(
    0xf3d0,
    fontFamily: CupertinoIcons.iconFont,
    fontPackage: CupertinoIcons.iconFontPackage,
  );
}
