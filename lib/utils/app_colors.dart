import 'package:flutter/cupertino.dart';

class AppColors {
  static final Color activeBlue = CupertinoColors.activeBlue;
  static final Color fadeBackground = CupertinoColors.systemBackground;
  static final Color red = CupertinoColors.systemRed;
  static final Color green = CupertinoColors.systemGreen;
  static final Color black = CupertinoColors.black;
}