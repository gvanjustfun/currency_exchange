import 'dart:convert';

import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:http/http.dart' as http;

class ExchangeRepository {
  final http.Client httpClient;

  final String _privatUrl = 'https://api.privatbank.ua/';

  const ExchangeRepository({this.httpClient});

  Future<List<ExchangeModel>> getPrivatCash() async {
    final response = await http.get(_privatUrl + 'p24api/pubinfo?json&exchange&coursid=5');
    if(response.statusCode == 200) {
      final data = json.decode(response.body) as List;
      return data.map((item) {
        return ExchangeModel.fromPrivatJson(item);
      }).toList();
    } else {
      throw new Exception('Failed to load');
    }
  }

  Future<List<ExchangeModel>> getPrivatOnline() async {
    final response = await http.get(_privatUrl + 'p24api/pubinfo?exchange&json&coursid=11');
    if(response.statusCode == 200) {
      final data = json.decode(response.body) as List;
      return data.map((item) {
        return ExchangeModel.fromPrivatJson(item);
      }).toList();
    } else {
      throw new Exception('Failed to load');
    }
  }

}