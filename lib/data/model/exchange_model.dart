import 'package:equatable/equatable.dart';

class ExchangeModel extends Equatable {

  final String currency;
  final String baseCurrency;
  final String buy;
  final String sale;

  ExchangeModel({String currency, String baseCurrency, String buy, String sale}) :
    this.currency = currency ?? '',
    this.baseCurrency = baseCurrency ?? '',
    this.buy = buy ?? '0',
    this.sale = sale ?? '0';

  factory ExchangeModel.fromPrivatJson(Map<String, dynamic> json) {
    return ExchangeModel(
      currency: json['ccy'],
      baseCurrency: json['base_ccy'],
      buy: json['buy'],
      sale: json['sale'],
    );
  }

  @override
  List<Object> get props => [currency, baseCurrency, buy, sale];

}