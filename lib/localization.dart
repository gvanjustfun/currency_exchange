import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'l10n/messages_all.dart';

//
//Use the following commands from project root to generate code for translations:
//
//1. Generate ARB files from Intl.message calls:
//flutter pub run intl_translation:extract_to_arb --output-dir=lib/l10n lib/localization.dart
//
//2. Generate code, used in application, from ARB files:
//flutter pub run intl_translation:generate_from_arb --output-dir=lib/l10n --no-use-deferred-loading lib/localization.dart lib/l10n/intl_*.arb
//
//Read more:
//1. intl_translation package https://pub.dev/packages/intl_translation
//2. Flutter documentation 
//https://flutter.dev/docs/development/accessibility-and-localization/internationalization
//

class AppLocalizations{

  final String localeName;

  AppLocalizations(this.localeName);

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static Future<AppLocalizations> load(Locale locale) {
    final String name = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String canonicalName = Intl.canonicalizedLocale(name);
    return initializeMessages(canonicalName).then((_) {
      return AppLocalizations(canonicalName);
    });
  }

  String get title {
    return Intl.message('Currency Exchange',
      locale: localeName,
    );
  }

  String get currency {
    return Intl.message('Currency',
      locale: localeName,
    );
  }

  String get sell {
    return Intl.message('Sell',
      locale: localeName,
    );
  }

  String get buy {
    return Intl.message('Buy',
      locale: localeName,
    );
  }

  String get calculatePurchase {
    return Intl.message('Calculate a purchase',
      locale: localeName,
    );
  }

  String get calculateSale {
    return Intl.message('Calculate a sale',
      locale: localeName,
    );
  }

  String get inputTitleBuy {
    return Intl.message('I want to buy',
      locale: localeName,
    );
  }

  String get inputTitleSale {
    return Intl.message('I want to sale',
      locale: localeName,
    );
  }

  String get resultTitleBuy {
    return Intl.message('You should pay',
      locale: localeName,
    );
  }

  String get resultTitleSale {
    return Intl.message('You will receive',
      locale: localeName,
    );
  }

  String get course {
    return Intl.message('Course',
      locale: localeName,
    );
  }

  String get privatOnline {
    return Intl.message('Privat Online',
      locale: localeName,
    );
  }

  String get privatCash {
    return Intl.message('Privat Cash',
      locale: localeName,
    );
  }

}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {

  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'uk'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return AppLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) {
    return false;
  }

}