import 'package:currency_exchange/data/repository/exchange_repository.dart';
import 'package:currency_exchange/localization.dart';
import 'package:currency_exchange/ui/buy/buy_page.dart';
import 'package:currency_exchange/ui/buy/model/calc_mode.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_bloc.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_event.dart';
import 'package:currency_exchange/ui/main/main_page.dart';
import 'package:currency_exchange/utils/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  BlocSupervisor.delegate = MyBlocDelegate();

  ExchangeRepository exchangeRepository = ExchangeRepository();
  

  runApp(MyApp(
    exchangeRepository: exchangeRepository,
  ));
}

class MyApp extends StatelessWidget {
  final ExchangeRepository exchangeRepository;

  const MyApp({Key key, this.exchangeRepository}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) =>
          ProviderBloc(exchangeRepository: exchangeRepository)
            ..add(ChangeProvider(ProviderType.PrivatOnline)),
      child: CupertinoApp(
        debugShowCheckedModeBanner: false,
        title: 'Currency Exchange',
        theme: CupertinoThemeData(
          primaryColor: AppColors.activeBlue,
        ),
        localizationsDelegates: [
          const AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', ''),
          const Locale('uk', ''),
        ],
        initialRoute: '/',
        routes: {
          '/': (context) => MainPage(),
          '/buy': (context) => BuyPage(calcMode: CalcMode.BUY),
          '/sale': (context) => BuyPage(calcMode: CalcMode.SELL),
        },
      ),
    );
  }
}

class MyBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}
