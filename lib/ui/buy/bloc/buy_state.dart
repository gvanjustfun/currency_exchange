import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:equatable/equatable.dart';

class BuyState extends Equatable {
  @override
  List<Object> get props => [];

}

class BuyCurrenciesLoaded extends BuyState {
  final List<String> currencies;
  final String currentCurrency;
  final ExchangeModel exchange;
  final double inputSum;
  final double resultSum;

  BuyCurrenciesLoaded(this.currencies, this.currentCurrency, 
    this.exchange, this.inputSum, this.resultSum);

  @override 
  List<Object> get props => [currencies, currentCurrency, 
    exchange, inputSum, resultSum];

}