import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:equatable/equatable.dart';

class BuyEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ExchangesLoaded extends BuyEvent {
  final List<ExchangeModel> exchanges;

  ExchangesLoaded(this.exchanges);

  @override 
  List<Object> get props => [exchanges];

}

class ChangeCurrency extends BuyEvent {
  final String currency;

  ChangeCurrency(this.currency);

  @override 
  List<Object> get props => [currency];

}

class SumValueChanged extends BuyEvent {
  final String sum;

  SumValueChanged(this.sum);

  @override 
  List<Object> get props => [sum];

}