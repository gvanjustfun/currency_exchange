import 'dart:async';

import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:currency_exchange/ui/buy/bloc/buy_event.dart';
import 'package:currency_exchange/ui/buy/bloc/buy_state.dart';
import 'package:currency_exchange/ui/buy/model/calc_mode.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_bloc.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BuyBloc extends Bloc<BuyEvent, BuyState> {
  StreamSubscription providerSubscription;
  CalcMode calcMode;

  List<ExchangeModel> _exchanges = [];
  List<String> _currencies = [];
  String _currency = '';
  ExchangeModel _exchange = ExchangeModel();
  double inputSum = 0;
  double resultSum = 0;

  BuyBloc(ProviderBloc providerBloc, this.calcMode) {
    providerSubscription = providerBloc.listen((state) {
      if (state is ProviderChanged) {
        add(ExchangesLoaded(state.exchanges));
      }
    });
  }

  String get currency => _currency;

  @override
  BuyState get initialState => rebuildState();

  @override
  Stream<BuyState> mapEventToState(BuyEvent event) async* {
    if (event is ExchangesLoaded) {
      this._exchanges = event.exchanges;
      _currencies = _exchanges.map((item) => item.currency).toList();
      if (_currency.isEmpty && _currencies.length > 0) {
        _currency = _currencies[0];
        _exchange = _exchanges[0];
      }
      yield rebuildState();
    } else if (event is ChangeCurrency) {
      this._currency = event.currency;
      this._exchange =
          _exchanges.firstWhere((item) => item.currency == _currency);
      yield rebuildState();
    } else if (event is SumValueChanged) {
      if (event.sum.isNotEmpty) {
        this.inputSum = double.parse(event.sum);
      } else {
        this.inputSum = 0;
      }
      yield rebuildState();
    }
  }

  BuyCurrenciesLoaded rebuildState() {
    double rate = double.parse(
          calcMode == CalcMode.BUY ? _exchange.sale : _exchange.buy);
      this.resultSum = rate * inputSum;
    return BuyCurrenciesLoaded(
        _currencies, _currency, _exchange, inputSum, resultSum);
  }

  @override
  Future<void> close() {
    providerSubscription.cancel();
    return super.close();
  }
}
