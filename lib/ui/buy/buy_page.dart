import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:currency_exchange/localization.dart';
import 'package:currency_exchange/ui/buy/bloc/buy_bloc.dart';
import 'package:currency_exchange/ui/buy/bloc/buy_event.dart';
import 'package:currency_exchange/ui/buy/bloc/buy_state.dart';
import 'package:currency_exchange/ui/buy/model/calc_mode.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_bloc.dart';
import 'package:currency_exchange/utils/app_colors.dart';
import 'package:currency_exchange/utils/app_icons.dart';
import 'package:currency_exchange/view/bottom_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class BuyPage extends StatelessWidget {
  final CalcMode calcMode;

  const BuyPage({Key key, @required this.calcMode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BuyBloc>(
      create: (context) =>
          BuyBloc(BlocProvider.of<ProviderBloc>(context), calcMode),
      child: _BuyPageWidget(
        calcMode: calcMode,
      ),
    );
  }
}

class _BuyPageWidget extends StatelessWidget {
  final CalcMode calcMode;

  const _BuyPageWidget({Key key, @required this.calcMode}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    var inputTitle = calcMode == CalcMode.BUY 
      ? AppLocalizations.of(context).inputTitleBuy 
      : AppLocalizations.of(context).inputTitleSale;
    var resultTitle = calcMode == CalcMode.BUY
      ? AppLocalizations.of(context).resultTitleBuy
      : AppLocalizations.of(context).resultTitleSale;

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text(calcMode == CalcMode.BUY
            ? AppLocalizations.of(context).calculatePurchase
            : AppLocalizations.of(context).calculateSale),
      ),
      child: SafeArea(
        child: Container(
          color: Color.fromRGBO(245, 245, 245, 1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(16, 16, 16, 8),
                child: Text(
                  '$inputTitle:',
                  style: TextStyle(
                    color: AppColors.activeBlue,
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: _AmountInput(),
                  ),
                  Expanded(
                    flex: 1,
                    child: _CurrencyPopup(),
                  ),
                ],
              ),
              _ExchangeRate(
                calcMode: calcMode,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(16, 24, 16, 0),
                child: Text(
                  '$resultTitle:',
                  style: TextStyle(
                    color: AppColors.activeBlue,
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              _ResultSum(
                calcMode: calcMode,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _AmountInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(16, 0, 8, 0),
      child: CupertinoTextField(
        keyboardType: TextInputType.number,
        autofocus: true,
        onChanged: (number) {
          BlocProvider.of<BuyBloc>(context).add(SumValueChanged(number));
        },
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 16,
        ),
      ),
    );
  }
}

class _CurrencyPopup extends StatelessWidget {
  List<Widget> _getPopupEntries(List<String> currencies) {
    List<Widget> entries = [];
    for (String currency in currencies) {
      Center entry = Center(
        child: Text(
          currency,
        ),
      );
      entries.add(entry);
    }
    return entries;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 0, 16, 0),
      child: Center(
        child: BlocBuilder<BuyBloc, BuyState>(
          builder: (context, state) {
            if (state is BuyCurrenciesLoaded) {
              List<String> currencies = state.currencies;
              String currentCurrency = state.currentCurrency;

              return GestureDetector(
                child: Row(
                  children: <Widget>[
                    Text(
                      currentCurrency,
                      style: TextStyle(
                          color: AppColors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    Icon(AppIcons.chevronDown)
                  ],
                ),
                onTap: () {
                  showCupertinoModalPopup(
                    context: context,
                    useRootNavigator: true,
                    builder: (BuildContext bContext) {
                      return BottomPicker(
                        child: CupertinoPicker(
                          itemExtent: 32,
                          scrollController: FixedExtentScrollController(
                            initialItem: currencies.indexOf(currentCurrency),
                          ),
                          backgroundColor: CupertinoColors.systemBackground
                              .resolveFrom(bContext),
                          onSelectedItemChanged: (int index) {
                            print(index);
                            String currency = currencies[index];
                            BlocProvider.of<BuyBloc>(context)
                                .add(ChangeCurrency(currency));
                          },
                          children: _getPopupEntries(currencies),
                        ),
                      );
                    },
                  );
                },
              );
            } else {
              return Text('');
            }
          },
        ),
      ),
    );
  }
}

class _ExchangeRate extends StatelessWidget {
  final numFormat;
  final CalcMode calcMode;

  _ExchangeRate({Key key, @required this.calcMode})
      : numFormat = NumberFormat('##0.00', 'en_US'),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BuyBloc, BuyState>(builder: (context, state) {
      if (state is BuyCurrenciesLoaded) {
        ExchangeModel exchange = state.exchange;
        String courseWord = AppLocalizations.of(context).course;
        String text;
        if (exchange != null) {
          String exchangeRate =
              calcMode == CalcMode.BUY ? exchange.sale : exchange.buy;
          String exchangeRateFmt = numFormat.format(double.parse(exchangeRate));
          text =
              '$courseWord: 1 ${exchange.currency} = $exchangeRateFmt ${exchange.baseCurrency}';
        } else {
          text = '$courseWord:';
        }

        return Padding(
          padding: EdgeInsets.fromLTRB(16, 8, 16, 0),
          child: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 14,
            ),
          ),
        );
      } else {
        return SizedBox.shrink();
      }
    });
  }
}

class _ResultSum extends StatelessWidget {
  final numFormat;
  final CalcMode calcMode;

  _ResultSum({Key key, @required this.calcMode})
      : numFormat = NumberFormat('###,##0.00', 'en_US'),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BuyBloc, BuyState>(builder: (context, state) {
      if (state is BuyCurrenciesLoaded) {
        String text;
        String resultSum =
            numFormat.format(state.resultSum).replaceAll(',', ' ');
        if (state.exchange != null) {
          text = '$resultSum ${state.exchange.baseCurrency}';
        } else {
          text = '';
        }
        return Padding(
          padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
          child: Text(
            text,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
        );
      } else {
        return SizedBox.shrink();
      }
    });
  }
}


