import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:currency_exchange/localization.dart';
import 'package:currency_exchange/ui/exchange/bloc/exchange_bloc.dart';
import 'package:currency_exchange/ui/exchange/bloc/exchange_state.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_bloc.dart';
import 'package:currency_exchange/utils/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class ExchangePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ExchangeBloc>(
      create: (context) => ExchangeBloc(BlocProvider.of<ProviderBloc>(context)),
      child: Container(
        color: Color.fromRGBO(245, 245, 245, 1),
        child: Stack(children: [
          Column(children: [
            _ExchangeHeader(),
            BlocBuilder<ExchangeBloc, ExchangeState>(builder: (context, state) {
              if (state is ExchangeListLoaded) {
                List<ExchangeModel> exchanges = state.exchangeList;
                return ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: exchanges.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _ExchangeListItem(exchange: exchanges[index]);
                    });
              } else {
                return Text('Empty');
              }
            })
          ]),
          _BottomButtons(),
        ]),
      ),
    );
  }
}

class _ExchangeHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 8.0, 0, 0),
      child: Row(children: [
        Expanded(
          child: Center(
            child: Text(
              AppLocalizations.of(context).buy,
              style: TextStyle(color: AppColors.red, fontSize: 13.0),
            ),
          ),
        ),
        Expanded(
          child: Center(
            child: Text(
              AppLocalizations.of(context).currency,
              style: TextStyle(color: AppColors.green, fontSize: 13.0),
            ),
          ),
        ),
        Expanded(
          child: Center(
            child: Text(
              AppLocalizations.of(context).sell,
              style: TextStyle(color: AppColors.red, fontSize: 13.0),
            ),
          ),
        ),
      ]),
    );
  }
}

class _ExchangeListItem extends StatelessWidget {
  final ExchangeModel exchange;
  final numFormat;

  _ExchangeListItem({Key key, this.exchange})
      : numFormat = NumberFormat('##0.00', 'en_US'),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Row(children: [
        Expanded(
          child: Center(
            child: Text(
              numFormat.format(double.parse(exchange.buy)),
              style: TextStyle(
                  color: AppColors.red,
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Expanded(
          child: Center(
            child: Text(
              exchange.currency,
              style: TextStyle(
                  color: AppColors.green,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Expanded(
          child: Center(
            child: Text(
              numFormat.format(double.parse(exchange.sale)),
              style: TextStyle(
                  color: AppColors.red,
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ]),
    );
  }
}

class _BottomButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.fromLTRB(16, 16, 8, 24),
                child: CupertinoButton(
                  padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                  child: Text(
                      AppLocalizations.of(context).calculatePurchase,
                      textAlign: TextAlign.center,
                    ),
                  color: AppColors.green,
                  onPressed: () {
                    Navigator.of(context).pushNamed('/buy');
                  },
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.fromLTRB(8, 16, 16, 24),
                child: CupertinoButton(
                  padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                  child: Text(
                      AppLocalizations.of(context).calculateSale,
                      textAlign: TextAlign.center,
                    ),
                  color: AppColors.green,
                  onPressed: () {
                    Navigator.of(context).pushNamed('/sale');
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
