import 'dart:async';

import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:currency_exchange/ui/exchange/bloc/exchange_event.dart';
import 'package:currency_exchange/ui/exchange/bloc/exchange_state.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_bloc.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ExchangeBloc extends Bloc<ExchangeEvent, ExchangeState> {

  StreamSubscription providerSubscription;

  ExchangeBloc(ProviderBloc providerBloc) {
    providerSubscription = providerBloc.listen((state) {
      if(state is ProviderChanged) {
        add(ExchangesLoaded(state.exchanges));
      }
    });
  }

  @override
  ExchangeState get initialState => ExchangeListLoaded(exchangeList: []);

  @override
  Stream<ExchangeState> mapEventToState(ExchangeEvent event) async* {
    if(event is ExchangesLoaded) {
      List<ExchangeModel> exchangeList = event.exchanges;
      yield ExchangeListLoaded(exchangeList: exchangeList);
    }
  }

  @override 
  Future<void> close() {
    providerSubscription.cancel();
    return super.close();
  }

}