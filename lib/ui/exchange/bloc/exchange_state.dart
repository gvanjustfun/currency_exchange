import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class ExchangeState extends Equatable {
  @override
  List<Object> get props => [];
}

class ExchangeListLoaded extends ExchangeState {
  final List<ExchangeModel> exchangeList;

  ExchangeListLoaded({@required this.exchangeList});

  @override 
  List<Object> get props => [exchangeList];

}