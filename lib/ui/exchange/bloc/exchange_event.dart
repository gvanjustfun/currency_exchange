import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:equatable/equatable.dart';

class ExchangeEvent extends Equatable {
  @override
  List<Object> get props => null;
}

class WidgetStart extends ExchangeEvent{}

class ExchangesLoaded extends ExchangeEvent {

  final List<ExchangeModel> exchanges;

  ExchangesLoaded(this.exchanges);

}