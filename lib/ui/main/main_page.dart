import 'package:currency_exchange/localization.dart';
import 'package:currency_exchange/ui/exchange/exchange_page.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_bloc.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_event.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_state.dart';
import 'package:currency_exchange/utils/app_icons.dart';
import 'package:currency_exchange/view/bottom_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _MainScaffold();
  }
}

class _MainScaffold extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      resizeToAvoidBottomInset: false,
      navigationBar: CupertinoNavigationBar(
        middle: BlocBuilder<ProviderBloc, ProviderState>(
          builder: (context, state) {
            return _AppBarTitle(
              providerType: state.providerType,
            );
          },
        ),
      ),
      child: SafeArea(
        child: Stack(
          children: <Widget>[
            ExchangePage(),
            _MainProgress(),
          ],
        ),
      ),
    );
  }
}

class _AppBarTitle extends StatelessWidget {
  final ProviderType providerType;
  final List<ProviderType> providerTypes = ProviderType.values;

  const _AppBarTitle({Key key, @required this.providerType}) : super(key: key);

  String _getTitleForProvider(BuildContext context, ProviderType providerType) {
    switch (providerType) {
      case ProviderType.PrivatCash:
        return AppLocalizations.of(context).privatCash;
      case ProviderType.PrivatOnline:
        return AppLocalizations.of(context).privatOnline;
    }
    return '';
  }

  List<Widget> _buildPopupEntries(BuildContext context) {
    List<Widget> entries = [];
    for (ProviderType type in providerTypes) {
      Widget entry = Center(
        child: Text(
          _getTitleForProvider(context, type),
        ),
      );
      entries.add(entry);
    }
    return entries;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProviderBloc, ProviderState>(
      builder: (context, state) {
        return GestureDetector(
          child: Row(
            children: <Widget>[
              Text(_getTitleForProvider(context, state.providerType)),
              Icon(AppIcons.chevronDown)
            ],
          ),
          onTap: () {
            showCupertinoModalPopup(
                context: context,
                builder: (BuildContext context) {
                  return BottomPicker(
                    child: CupertinoPicker(
                      itemExtent: 32,
                      scrollController: FixedExtentScrollController(
                        initialItem: providerTypes.indexOf(providerType),
                      ),
                      backgroundColor:
                          CupertinoColors.systemBackground.resolveFrom(context),
                      onSelectedItemChanged: (index) {
                        ProviderType providerType = providerTypes[index];
                        BlocProvider.of<ProviderBloc>(context)
                            .add(ChangeProvider(providerType));
                      },
                      children: _buildPopupEntries(context),
                    ),
                  );
                });
          },
        );
      },
    );
  }
}

class _MainProgress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProviderBloc, ProviderState>(builder: (context, state) {
      if (state is ProviderLoading) {
        return Container(
            color: Color.fromRGBO(0, 0, 0, 0.12),
            child: Center(
              child: CupertinoActivityIndicator(),
            ));
      } else {
        return SizedBox.shrink();
      }
    });
  }
}
