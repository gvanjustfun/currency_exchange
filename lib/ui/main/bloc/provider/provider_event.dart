import 'package:currency_exchange/ui/main/bloc/provider/provider.dart';
import 'package:equatable/equatable.dart';

class ProviderEvent extends Equatable {
  @override
  List<Object> get props => [];

}

class ChangeProvider extends ProviderEvent {
  final ProviderType providerType;

  ChangeProvider(this.providerType);

  @override 
  List<Object> get props => [providerType];

}