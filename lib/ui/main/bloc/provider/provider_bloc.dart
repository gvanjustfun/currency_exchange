import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:currency_exchange/data/repository/exchange_repository.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_event.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProviderBloc extends Bloc<ProviderEvent, ProviderState> {

  ExchangeRepository exchangeRepository;
  ProviderType _providerType;
  List<ExchangeModel> exchanges;

  ProviderBloc({this.exchangeRepository}) : 
    assert(exchangeRepository != null),
    _providerType = ProviderType.PrivatOnline,
    exchanges = [];

  @override
  ProviderState get initialState => ProviderChanged(providerType: ProviderType.PrivatOnline, exchanges: []);

  @override
  Stream<ProviderState> mapEventToState(ProviderEvent event) async* {
    if(event is ChangeProvider && state is ProviderChanged) {
      _providerType = event.providerType;
      yield ProviderLoading(providerType: _providerType);
      if(_providerType == ProviderType.PrivatCash) {
        exchanges = await exchangeRepository.getPrivatCash();
        exchanges = exchanges.where((item) => item.currency != 'BTC').toList();
        yield ProviderChanged(providerType: _providerType, exchanges: exchanges);
      } else
      if(_providerType == ProviderType.PrivatOnline) {
        exchanges = await exchangeRepository.getPrivatOnline();
        exchanges = exchanges.where((item) => item.currency != 'BTC').toList();
        yield ProviderChanged(providerType: _providerType, exchanges: exchanges);
      } else {
        yield ProviderChanged(providerType: _providerType, exchanges: []);
      }
    }
  }

}