import 'package:currency_exchange/data/model/exchange_model.dart';
import 'package:currency_exchange/ui/main/bloc/provider/provider.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class ProviderState extends Equatable {

  final ProviderType providerType;

  ProviderState({@required this.providerType});

  @override
  List<Object> get props => [providerType];

}

class ProviderLoading extends ProviderState {
  ProviderLoading({@required ProviderType providerType}) : super(providerType: providerType);
}

class ProviderChanged extends ProviderState {
  final List<ExchangeModel> exchanges;

  ProviderChanged({@required ProviderType providerType, @required this.exchanges}) : super(providerType: providerType);

  @override 
  List<Object> get props => [exchanges];

}