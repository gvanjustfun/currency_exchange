// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a uk locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'uk';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Buy" : MessageLookupByLibrary.simpleMessage("Купівля"),
    "Calculate a purchase" : MessageLookupByLibrary.simpleMessage("Розрахувати покупку"),
    "Calculate a sale" : MessageLookupByLibrary.simpleMessage("Розрахувати продаж"),
    "Course" : MessageLookupByLibrary.simpleMessage("Курс"),
    "Currency" : MessageLookupByLibrary.simpleMessage("Валюта"),
    "Currency Exchange" : MessageLookupByLibrary.simpleMessage("Currency Exchange"),
    "I want to buy" : MessageLookupByLibrary.simpleMessage("Я хочу купити"),
    "I want to sale" : MessageLookupByLibrary.simpleMessage("Я хочу продати"),
    "Privat Cash" : MessageLookupByLibrary.simpleMessage("Приват готівка"),
    "Privat Online" : MessageLookupByLibrary.simpleMessage("Приват онлайн"),
    "Sell" : MessageLookupByLibrary.simpleMessage("Продаж"),
    "You should pay" : MessageLookupByLibrary.simpleMessage("Ви заплатите"),
    "You will receive" : MessageLookupByLibrary.simpleMessage("Ви отримаєте")
  };
}
