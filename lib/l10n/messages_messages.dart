// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'messages';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Buy" : MessageLookupByLibrary.simpleMessage("Buy"),
    "Calculate a purchase" : MessageLookupByLibrary.simpleMessage("Calculate a purchase"),
    "Calculate a sale" : MessageLookupByLibrary.simpleMessage("Calculate a sale"),
    "Course" : MessageLookupByLibrary.simpleMessage("Course"),
    "Currency" : MessageLookupByLibrary.simpleMessage("Currency"),
    "Currency Exchange" : MessageLookupByLibrary.simpleMessage("Currency Exchange"),
    "I want to buy" : MessageLookupByLibrary.simpleMessage("I want to buy"),
    "I want to sale" : MessageLookupByLibrary.simpleMessage("I want to sale"),
    "Privat Cash" : MessageLookupByLibrary.simpleMessage("Privat Cash"),
    "Privat Online" : MessageLookupByLibrary.simpleMessage("Privat Online"),
    "Sell" : MessageLookupByLibrary.simpleMessage("Sell"),
    "You should pay" : MessageLookupByLibrary.simpleMessage("You should pay"),
    "You will receive" : MessageLookupByLibrary.simpleMessage("You will receive")
  };
}
